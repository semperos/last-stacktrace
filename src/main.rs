use quicli::prelude::*;
use serde::Deserialize;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
struct Cli {
    #[structopt(long = "file", short = "f")]
    file: Option<String>,

    #[structopt(long = "timestamp-index", short = "i", default_value = "24")]
    timestamp_index: usize,

    #[structopt(long = "num-stacktraces", short = "n", default_value = "1")]
    num_stacktraces: usize,
}

#[derive(Debug, Deserialize)]
struct StacktraceLog {
    stacktrace: Option<String>,
}

fn main() -> CliResult {
    let args = Cli::from_args();
    let file = match args.file {
        Some(f) => f,
        None => {
            let file = std::env::var_os("LAST_STACKTRACE_LOG");
            match file {
                Some(f) => f.into_string().unwrap(),
                None => {
                    eprintln!("You must either supply --file or define LAST_STACKTRACE_LOG");
                    std::process::exit(1);
                }
            }
        }
    };
    let idx = &args.timestamp_index;
    // TODO If num_stacktraces, perhaps pull the closure out and use find_map vs. filter
    let st_opt = read_file(&file)?.lines().rev().find_map(|line| {
        let (_first, last) = line.split_at(*idx);
        let st_log_result: Result<StacktraceLog, serde_json::Error> = serde_json::from_str(last);
        match st_log_result {
            Ok(st_log) => {
                if let Some(stacktrace) = st_log.stacktrace {
                    Some(stacktrace)
                } else {
                    None
                }
            }
            Err(e) => {
                eprintln!("{:?}", e);
                None
            }
        }
    });
    if let Some(st) = st_opt {
        println!("{}", st);
    } else {
        eprintln!("No stacktrace log entries found.");
    }
    Ok(())
}
